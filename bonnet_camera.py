# Copyright (c) 2020 William Salmon
# Author: William Salmon
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

import board
import busio
from digitalio import DigitalInOut, Direction, Pull
from PIL import Image, ImageDraw, ImageFont, ImageOps
import adafruit_ssd1306
import atexit


def main():
    # Create the I2C interface.
    i2c = busio.I2C(board.SCL, board.SDA)
    # Create the SSD1306 OLED class.
    disp = adafruit_ssd1306.SSD1306_I2C(128, 64, i2c)

    def cls():
        disp.fill(0)
        disp.show()
    atexit.register(cls)
    cls()

    width = disp.width
    height = disp.height
    image = Image.new('1', (width, height))

    # Get drawing object to draw on image.
    draw = ImageDraw.Draw(image)

    # Draw a black filled box to clear the image.

    # specified font size 
    font = ImageFont.truetype(r'/usr/share/fonts/truetype/freefont/FreeMono.ttf', 10)  

    text = 'Starting up'
    draw.text((0, 0), text, font = font, align ="left", fill=1)  
    disp.image(image)
    disp.show()

    import cv2
    from uuid import uuid1
    import time
    import numpy as np



    # Input pins:
    button_A = DigitalInOut(board.D5)
    button_A.direction = Direction.INPUT
    button_A.pull = Pull.UP

    button_B = DigitalInOut(board.D6)
    button_B.direction = Direction.INPUT
    button_B.pull = Pull.UP

    button_L = DigitalInOut(board.D27)
    button_L.direction = Direction.INPUT
    button_L.pull = Pull.UP

    button_R = DigitalInOut(board.D23)
    button_R.direction = Direction.INPUT
    button_R.pull = Pull.UP

    button_U = DigitalInOut(board.D17)
    button_U.direction = Direction.INPUT
    button_U.pull = Pull.UP

    button_D = DigitalInOut(board.D22)
    button_D.direction = Direction.INPUT
    button_D.pull = Pull.UP

    button_C = DigitalInOut(board.D4)
    button_C.direction = Direction.INPUT
    button_C.pull = Pull.UP


    # Clear display.
    disp.fill(0)
    disp.show()

    # Create blank image for drawing.
    # Make sure to create image with mode '1' for 1-bit color.


    cap = cv2.VideoCapture(0)


    def bgr8_to_jpeg(value, quality=95):
        return bytes(cv2.imencode('.jpg', value)[1])
    def save(frame):
        filename = "livecap/{}.jpg".format(uuid1())
        with open(filename, 'wb') as fl:
            fl.write(bgr8_to_jpeg(frame))



    size = 128, 64

    while True:
        draw.rectangle((0, 0, width, height), outline=0, fill=0)
        text = 'Ready for a picture'
        draw.text((0, 0), text, font = font, align ="left", fill=1)  

        if button_A.value: # button is released
            pass    
        else: # button is pressed:
            text = 'Saved a picture'
            draw.text((5, 8), text, font = font, align ="left", fill=1)  
            disp.image(image)
            disp.show()
            for ii in range(10):
                _, frame = cap.read()
            save(frame)
            im = Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
            im.thumbnail(size, Image.ANTIALIAS)
            print(im.size)
            im = im.convert("1")
            new_size = im.size
            desired_size = 128,64
            delta_w = desired_size[0] - new_size[0]
            delta_h = desired_size[1] - new_size[1]
            padding = (delta_w//2, delta_h//2, delta_w-(delta_w//2), delta_h-(delta_h//2))
            im = ImageOps.expand(im, padding)
            print(im.size)
            disp.image(im)
            disp.show()
            time.sleep(2)
            while button_B.value and button_A.value:
                time.sleep(0.05)
            continue


        disp.image(image)
        disp.show()

if __name__=="__main__":
    main()
