from setuptools import setup

setup(
    name='bonnetCam',
    entry_points={
        'console_scripts': [
            'bonnetCam = bonnet_camera:main',
        ],
    }
)
